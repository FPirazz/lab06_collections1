package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.HashMap;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {
	
	final static int ELEMS1 = 100_000;
	final static int TO_MS1 = 1_000_000;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	ArrayList<Integer> listA1 = new ArrayList<>();
    	
    	for(int i = 1_000; i < 2_000; i++) {
    		
    		listA1.add(i);
    		
    	}
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	LinkedList<Integer> listL1 = new LinkedList<>();
    	
    	listL1.addAll(listA1);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	int tmp = listA1.get( listA1.size() - 1 );
    	
    	listA1.set( listA1.size() -1 , listA1.get( listA1.size() - listA1.size() ) );	
    	
    	listA1.set( listA1.size() - listA1.size() , tmp );
    	/*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (int index:listA1) {
    		System.out.println(index);
    	}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime();
    	
    	for (int i = 1; i <= ELEMS1; i++) {
    		
    		listA1.add(i);
    		listL1.add(i);
    		
    	}
    	
    	time = System.nanoTime() - time;
    	
    	System.out.println("Adding " + ELEMS1
                + " elements into a LinkedList and an ArrayList, it took " + time
                + " ns (" + time / TO_MS1 + "ms)");
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	
    	time = System.nanoTime();
    	
    	for (int i = 0; i < (listL1.size() / 2); i++) {
    		
    	}
    	
    	time = System.nanoTime() - time;
    	
    	System.out.println("Reading " + listL1.size()
                + " elements of a LinkedList and an ArrayList, it took " + time
                + " ns (" + time / TO_MS1 + "ms)");
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	HashMap<String, Long> map1 = new HashMap<>();
    	
    	map1.putIfAbsent("Africa", 1_110_635_000L);
    	map1.putIfAbsent("Americas", 972_005_000L);
    	map1.putIfAbsent("Antarctica", 0L);
    	map1.putIfAbsent("Asia", 4_298_723_000L);
    	map1.putIfAbsent("Europe", 742_452_000L);
    	map1.putIfAbsent("Oceania", 38_304_000L);
    	
    	for (Entry<String, Long> index: map1.entrySet()) {
    		System.out.println(index);
    	}
        /*
         * 8) Compute the population of the world
         */
    }
}
